<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <script type="text/javascript">
        function setCarForm(carData) {
            $('#color').val(carData.color);
            $('#make').val(carData.make);
            $('#model').val(carData.model);
            $('#type').val(carData.type);
            $('#name').val(carData.name);
            $('#carform').attr('action', "{{ url('cars') }}/"+carData.id);
            $('#formMethod').val('PUT');

            $('#carModal').removeClass('invisible');
        }
        $(document).ready( function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#car-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('cars') }}",
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'make', name: 'make' },
                    { data: 'model', name: 'model' },
                    { data: 'type', name: 'type' },
                    { data: 'color', name: 'color' },
                    { data: 'action', name: 'action', orderable: false},
                ],
                order: [[0, 'asc']],
                fnDrawCallback: function( settings, json ) {
                    $('.removeCar').on('click', function(e){
                        let carId = $(this).attr('data-id');
                        console.log('Deleting Car ID: '+carId);
                        $.ajax({
                            type:'DELETE',
                            url: "{{ url('cars')}}/"+carId,
                            success: (data) => {
                                console.log(data);
                                let carTable = $('#car-datatable').dataTable();
                                carTable.fnDraw(false);
                            },
                            error: function(data){
                                console.log(data);
                            }
                        });
                    });
                    $('.updateCar').on('click', function(e){
                        let carId = $(this).attr('data-id');
                        console.log('Getting Car Data ID: '+carId);
                        $.ajax({
                            type:'GET',
                            url: "{{ url('cars')}}/"+carId,
                            success: (data) => {
                                console.log(data);
                                setCarForm(data);
                            },
                            error: function(data){
                                console.log(data);
                            }
                        });
                    });
                }
            });
            $('.openModal').on('click', function(e){
                $('#carform').attr('action', "{{ url('cars') }}");
                $('#formMethod').val('POST');
                $('#color').val('');
                $('#make').val('');
                $('#model').val('');
                $('#type').val('');
                $('#name').val('');
                $('#carModal').removeClass('invisible');
            });
            $('.closeModal').on('click', function(e){
                $('#carModal').addClass('invisible');
            });
            $('#carform').submit(function(e) {
                e.preventDefault();
                let formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: $(this).attr('action'),
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        $('#carModal').addClass('invisible');
                        let carTable = $('#car-datatable').dataTable();
                        carTable.fnDraw(false);
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        });
    </script>
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="py-4"><button type="button" class="focus:outline-none openModal text-white text-sm py-2.5 px-5 mt-5 mx-5  rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Add Vehicle</button></div>

            <table class="table table-bordered py-4" id="car-datatable">
            <thead>
            <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Make</th>
            <th>Model</th>
            <th>Type</th>
            <th>Color</th>
            <th></th>
            </tr>
            </thead>
            </table>
        </div>
    </div>
<div class="fixed z-10 inset-0 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true" id="carModal">
                <form class="px-6 pb-4 space-y-6 lg:px-8 sm:pb-6 xl:pb-8" action="javascript:void(0)" id="carform" name="carform" method="POST" enctype="multipart/form-data">
<input type="hidden" id="formMethod" name="_method" value="PUT">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
                <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
                <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                    <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div class="sm:flex sm:items-start">
                            <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                <h3 class="text-xl font-medium text-gray-900 dark:text-white">Add New Car</h3>
                                <div>
                                    <label for="name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Vehicle Name</label>
                                    <input type="text" name="name" id="name" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="name" required>
                                </div>
                                <div>
                                    <label for="make" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Make</label>
                                    <input type="text" name="make" id="make" placeholder="Ford, Chevy, etc" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required>
                                </div>
                                <div>
                                    <label for="model" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Model</label>
                                    <input type="text" name="model" id="model" placeholder="Grand Am" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required>
                                </div>
                                <div>
                                    <label for="type" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Type</label>
                                    <input type="text" name="type" id="type" placeholder="SUV, Pickup, etc" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required>
                                </div>
                                <div>
                                    <label for="color" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Color</label>
                                    <input type="text" name="color" id="color" placeholder="Red, Blue, etc" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <button type="submit" class="saveModal w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">
                        Save Vehicle
                    </button>
                    <button type="button" class="closeModal mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                        Cancel
                    </button>
                </div>
            </div>
                </form>
        </div>
</x-app-layout>
