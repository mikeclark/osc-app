# OSC-APP

## About
This app was created as a demo CRUD App in Laravel

### Steps to get up and running
Clone this repo
```
git clone https://gitlab.com/mikeclark/osc-app.git
```

Create .env
```
cp .env.example .env
```

Setup initial composer/sail requirements in container
```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
```

Start Sail (docker containers)
```
cd osc-app
bash vendor/bin/sail up
```

Generate Key
```
bash vendor/bin/sail php artisan key:generate
```

Run Database Migrationss
```
bash vendor/bin/sail php artisan migrate
```

Install NPM Packages and setup ui
```
bash vendor/bin/sail npm run dev
```

Now you should be able to browse to http://127.0.0.1/
