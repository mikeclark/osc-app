<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Car;
use DataTables;


class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->ajax()) {
            return datatables()->of(Car::select('*')->where('user_id', '=', $request->user()->id))
                ->addColumn('action', '<a href="#" class="removeCar px-4 py-1 text-sm text-red-400 rounded-full" data-id="{{$id}}">Delete</a><a href="#" class="updateCar px-4 py-1 text-sm text-red-400 rounded-full" data-id="{{$id}}">Update</a>')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->ajax()) {
            $car = new Car;
            $car->name = $request->name;
            $car->type = $request->type;
            $car->make = $request->make;
            $car->model = $request->model;
            $car->color = $request->color;
            $car->user_id = $request->user()->id;
            $car->save();
            return Response()->json($car);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = Car::find($id);
        return Response()->json($car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (request()->ajax()) {
            $car = Car::find($id);
            $car->name = $request->name;
            $car->type = $request->type;
            $car->make = $request->make;
            $car->model = $request->model;
            $car->color = $request->color;
            $car->save();
            return Response()->json($car);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::find($id)->delete();
        return Response()->json($car);
    }
}
